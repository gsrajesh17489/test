
public class WSResult {
	private int _id;
	private long _time;
	private boolean _status;
	
	public WSResult(int id, long time, boolean status){
		_id = id;
		_time = time;
		_status = status;
	}

	public long getTime() {
		return _time;
	}
}
