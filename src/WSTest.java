import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class WSTest {
	public static void main(String[] args){
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		
		Set<WSCallable> callables = new HashSet<WSCallable>();
		int n = 100;
		boolean[] status = new boolean[n];
		for(int i = 0; i < n; i++){
			callables.add(new WSCallable(i));
		}
		
		try {
			List<Future<WSResult>> futures = executorService.invokeAll(callables);
			long[] results = new long[n];
			for(int i = 0; i < futures.size(); i++){
				Future<WSResult> fr = futures.get(i);
				WSResult wr = fr.get();
				results[i] = wr.getTime();
			}
			executorService.shutdown();
			
			Arrays.sort(results);
			Calculator c = new Calculator(results);
			c.PrintResults();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
