
public class Calculator {
	private long[] numbers;
	
	public Calculator(long[] numbers){
		this.numbers = numbers;
	}
	
	private long Sum(){
		long sum = 0;
		for(int i = 0; i < numbers.length; i++)
			sum+= numbers[i];
		return sum;
	}
	
	private double Mean(){
		return Sum()/numbers.length;
	}
	
	private double Variance(){
		double mean = Mean();
		double variance = 0;
		for(int i = 0; i < numbers.length; i++)
			variance += Math.pow(numbers[i] - mean, 2);
		return variance;
	}
	
	private double StandardDeviation(){
		return Math.sqrt(Variance());
	}
	
	public void PrintResults(){
		System.out.println("Mean : " + Mean());
		System.out.println("Standard Deviation : " + StandardDeviation());
		PrintPercentile();
	}
	
	public void PrintPercentile(){
		for(int i = 10; i <= 100; i+=10){
			
			long percentile = 0;
			if(i == 100){
				percentile = numbers[numbers.length-1];
			}
			else {
				double d = (i*numbers.length)/100;
				int index = (int)d;
				
				if(d % 1 == 0){					
					percentile = (numbers[index] + numbers[index+1])/2;
				}
				else {
					percentile = numbers[index + 1];
				}
			}
			
			
			
			System.out.println(i + "th percentile = " + percentile);
		}
	}
}
