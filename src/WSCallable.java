import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;

public class WSCallable implements Callable<WSResult>{
	private int _threadId;
	
	public WSCallable(int i){
		_threadId = i;
	}
	
	private String getResponse(URL obj){
		HttpURLConnection getCon;
		String stringResponse = "";
		try {
			getCon = (HttpURLConnection) obj.openConnection();
			
			getCon.setRequestMethod("GET");
			getCon.setRequestProperty("X-Surya-Email-Id", "gsrajesh89@gmail.com");
			
			int getResponseCode = getCon.getResponseCode();
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(getCon.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			stringResponse = response.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			stringResponse = null;
		}
		
		return stringResponse;		
	}
	
	private String getPostResponse(URL obj, String request){
		HttpURLConnection postCon;
		String stringResponse = "";
		try {
			postCon = (HttpURLConnection) obj.openConnection();
			postCon.setRequestMethod("POST");
			postCon.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(postCon.getOutputStream());
			wr.writeBytes(request);
			wr.flush();
			wr.close();
			
			int postResponseCode = postCon.getResponseCode();
			
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(postCon.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			stringResponse = response.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			stringResponse = null;
		}
		
		
		return stringResponse;
	}

	@Override
	public WSResult call() throws Exception {
		// TODO Auto-generated method stub
		long startTime = System.nanoTime();
		String url = "http://surya-interview.appspot.com/message";
		URL obj;
		boolean status = false;
		
		try {
			obj = new URL(url);
			String response = getResponse(obj);
			String postResponse = getPostResponse(obj,response);
			status = true;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		}
		
		long endTime = System.nanoTime();
		
		return new WSResult(_threadId, endTime - startTime, status);
	}
}
